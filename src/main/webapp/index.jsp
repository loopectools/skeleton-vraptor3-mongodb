<%@ page contentType="text/html; charset=UTF-8"%>

<div>
	<h1>Skeleton Project</h1>
	<h3>Reminders:</h3>
	<ul>
		<li>Change artifactId, name, and description tags in pom.xml</li>
		<li>Change packages in src/main/java</li>
		<li>Configure MongoDB environment</li>
		<li>Change line 7 in src/main/resources/log4j.properties</li>
		<li>Talk is cheap, so...CODE MAN, CODE!</li>
	</ul>

	<small>Developed by <a href="http://caiquerodrigues.github.com"
		target="_blank">Caique Peixoto</a>@<a href="http://loopec.com.br"
		target="_blank">Loop EC</a></small>
</div>

