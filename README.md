# Skeleton VRaptor3

## Introduction

Skeleton for web projects using:
* Maven 3.0.3
* VRaptor 3.5.1
* MongoDB 2.11.1
* JUnit 4.11
* Selenium HQ 2.28.0
* Mockito 1.9.5

## HowTo start a new project?

* Change artifactId, name, and description tags in pom.xml.  
* Change packages in src/main/java.   
* Configure MongoDB   
* Change line 7 in src/main/resources/log4j.properties.    
* Talk is cheap, so...CODE MAN, CODE!    

Developed by [Caique Peixoto](http://caiquerodrigues.github.com)@[Loop EC](http://loopec.com.br)